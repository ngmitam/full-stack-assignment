import React from "react";
import {
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Paper,
} from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";

import "./style.css";

export interface ItemProps {
	/**
	 * The label of the item.
	 */
	label: string;
	/**
	 * The price of the item.
	 */
	price: string;
	/**
	 * The description of the item.
	 */
	description?: string;
	/**
	 * The tags of the item.
	 */
	tags?: string[];
}

/**
 * Renders a Item component based on the provided item.
 *
 * @param {ItemProps} item - The item to display in the Item component.
 * @return {JSX.Element} The rendered Item component.
 */
export default function Item(item: ItemProps) {
	return (
		<Paper style={{ width: "100%" }}>
			<ListItemButton className="buy-menu-item">
				<div className="buy-menu-item-header">
					<ListItemIcon className="buy-menu-item-header-icon">
						<AddShoppingCartIcon />
					</ListItemIcon>
					<ListItemText
						primary={item.label}
						className="buy-menu-item-header-label"
					/>
					<ListItemText
						primary={item.price}
						className="buy-menu-item-header-price"
					/>
				</div>
				{item.description && (
					<div className="buy-menu-item-description">
						<ListItemText primary={item.description} />
					</div>
				)}
				{item?.tags?.length && (
					<div className="buy-menu-item-tags">
						{item.tags.map((tag, index) => (
							<ListItemText
								key={index}
								primary={tag}
								className="buy-menu-item-tag"
							/>
						))}
					</div>
				)}
			</ListItemButton>
		</Paper>
	);
}
