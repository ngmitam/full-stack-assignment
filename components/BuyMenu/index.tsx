import React from "react";
import { List, ListItem } from "@mui/material";

import Item from "./Item";

import "./style.css";

import type { ItemProps } from "./Item";

export interface BuyMenuProps {
	/**
	 * The items to display in the BuyMenu component.
	 */
	items?: ItemProps[];
}

/**
 * Renders a BuyMenu component based on the provided items.
 *
 * @param {BuyMenuProps} items - The items to display in the BuyMenu component.
 *
 * @return {JSX.Element} The rendered BuyMenu component.
 */
export default function BuyMenu({ items }: BuyMenuProps) {
	return (
		<List className="buy-menu">
			{items?.map((item, index) => (
				<ListItem
					key={index}
					disablePadding
					className="buy-menu-item-container"
				>
					<Item {...item} />
				</ListItem>
			))}
		</List>
	);
}
