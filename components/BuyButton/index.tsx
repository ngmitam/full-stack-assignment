import { useEffect, useRef, useState } from "react";
import { IconButton, Paper } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import Button from "@mui/material/Button";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import BuyModal from "../BuyModal";
import "./style.css";
import type { BuyModalProps } from "../BuyModal";

export interface BuyButtonProps {
	/**
	 * The onClick function of the BuyButton component.
	 */
	onClick: () => void;
	items: BuyModalProps["items"];
}

/**
 * Renders a BuyButton component based on the provided onClick prop.
 *
 * @param {BuyButtonProps} onClick - The onClick function of the BuyButton component.
 * @param {BuyModalProps["items"]} items - The items to display in the BuyModal component.
 * @return {JSX.Element} The rendered BuyButton component.
 */
export default function BuyButton({ onClick, items }: BuyButtonProps) {
	const [open, setOpen] = useState(false);
	const [buttonPosition, setButtonPosition] = useState({
		x: 0,
		y: 0,
		bottom: 0,
		right: 0,
		windowWidth: 0,
		windowHeight: 0,
	});
	const [buyMenuPosition, setBuyMenuPosition] = useState({
		x: 0,
		y: 0,
		bottom: 0,
		right: 0,
		windowWidth: 0,
		windowHeight: 0,
	});
	const buttonRef = useRef<HTMLDivElement>(null);

	const buyMenuRef = useRef<HTMLDivElement>(null);

	// Get position
	useEffect(() => {
		const updatePosition = () => {
			if (buttonRef.current) {
				const rect = buttonRef.current.getBoundingClientRect();
				console.log(rect);
				setButtonPosition({
					x: rect.x,
					y: rect.y,
					bottom: rect.bottom,
					right: rect.right,
					windowWidth: window.innerWidth,
					windowHeight: window.innerHeight,
				});
			}
		};

		updatePosition();

		window.addEventListener("resize", updatePosition);

		return () => {
			window.removeEventListener("resize", updatePosition);
		};
	}, []);

	useEffect(() => {
		const updateBuyMenuPosition = () => {
			if (buyMenuRef.current) {
				const rect = buyMenuRef.current.getBoundingClientRect();
				console.log(rect);
				if (rect.bottom >= buttonPosition.bottom) return;
				setBuyMenuPosition({
					x: rect.x,
					y: rect.y,
					bottom: rect.bottom,
					right: rect.right,
					windowWidth: window.innerWidth,
					windowHeight: window.innerHeight,
				});
			}
		};
		updateBuyMenuPosition();

		window.addEventListener("resize", updateBuyMenuPosition);

		return () => {
			window.removeEventListener("resize", updateBuyMenuPosition);
		};
	});

	const handleClose = () => {
		setOpen(false);
	};
	const handleOpen = () => {
		setOpen(true);
	};

	return (
		<div ref={buttonRef} className="buy-button-container">
			<BuyModal
				open={open}
				onClose={handleClose}
				onClick={onClick}
				buttonPosition={buttonPosition}
				items={items}
			/>

			{open ? (
				<div className="buy-modal-close-container">
					<Paper className="buy-modal-close">
						<IconButton onClick={handleClose}>
							<CloseIcon />
						</IconButton>
					</Paper>
				</div>
			) : (
				<Button
					variant="contained"
					onClick={handleOpen}
					className="buy-button"
					startIcon={<AddShoppingCartIcon />}
				>
					Buy
				</Button>
			)}
		</div>
	);
}
