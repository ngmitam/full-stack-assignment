import { useEffect, useRef, useState } from "react";
import { Slide, Paper } from "@mui/material";

import Modal from "@mui/material/Modal";

import IconButton from "@mui/material/IconButton";

import CloseIcon from "@mui/icons-material/Close";

import BuyMenu from "../BuyMenu";

import "./style.css";

import type { BuyMenuProps } from "../BuyMenu";

export interface BuyModalProps {
	/**
	 * The open state of the BuyModal component.
	 */
	open: boolean;
	/**
	 * The position of the button which triggers the BuyModal component.
	 */
	buttonPosition: {
		x: number;
		y: number;
		bottom: number;
		right: number;
		windowWidth: number;
		windowHeight: number;
	};
	/**
	 * The onClose function of the BuyModal component.
	 */
	onClose?: () => void;
	/**
	 * The onClick function of the BuyModal component.
	 */
	onClick?: () => void;
	/**
	 * The items to display in the BuyMenu component.
	 */
	items?: BuyMenuProps["items"];
}

/**
 * Renders a BuyModal component based on the provided open and onClose props.
 *
 * @param {BuyModalProps} open - The open state of the BuyModal component.
 * @param {BuyModalProps} onClose - The onClose function of the BuyModal component.
 * @param {BuyModalProps} onClick - The onClick function of the BuyModal component.
 * @param {BuyModalProps} items - The items to display in the BuyMenu component.
 * @return {JSX.Element} The rendered BuyModal component.
 */
export default function BuyModal({
	open,
	buttonPosition,
	onClose,
	onClick,
	items,
}: BuyModalProps) {
	return (
		<Modal open={open} onClose={onClose} closeAfterTransition>
			<Slide in={open} direction="up" mountOnEnter unmountOnExit>
				<div className="buy-modal-container">
					<BuyMenu items={items} />

					<Paper
						className="buy-modal-close"
						style={{
							marginTop: buttonPosition.y,
							marginRight:
								buttonPosition.windowWidth -
								buttonPosition.right,
						}}
					>
						<IconButton onClick={onClose}>
							<CloseIcon />
						</IconButton>
					</Paper>
				</div>
			</Slide>
		</Modal>
	);
}
