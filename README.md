## 1. Website

Start dev server

```bash
npm run dev
```

Build production

```bash
npm run build
```

Start production server

```bash
npm run start
```

## 2. Design System Component

Run server

```bash
npm run storybook
```

Build production

```bash
npm run build-storybook
```

## 3. NodeJS Back-end

Run unit test

```bash
npm test
```
