import type { Meta, StoryObj } from "@storybook/react";
import { fn } from "@storybook/test";
import Item from "../../components/BuyMenu/Item";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
	title: "System Design/BuyMenu/Item",
	component: Item,
	parameters: {
		// Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
		layout: "centered",
	},
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ["autodocs"],
} satisfies Meta<typeof Item>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Primary: Story = {
	args: {
		label: "Button text",
		price: "price",
	},
};

export const Full: Story = {
	args: {
		label: "Button text",
		price: "price",
		description: "An optional description",
		tags: ["optional tag"],
	},
};

export const OnlyTag: Story = {
	args: {
		label: "Button text",
		price: "price",
		tags: ["only tag"],
	},
};

export const OnlyDescription: Story = {
	args: {
		label: "Button text",
		price: "price",
		description: "Description only",
	},
};
