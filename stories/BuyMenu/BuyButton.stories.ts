import type { Meta, StoryObj } from "@storybook/react";
import { fn } from "@storybook/test";
import BuyButton from "./BuyButtonContainer";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
	title: "System Design/BuyButton",
	component: BuyButton,
	parameters: {
		// Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
		layout: "centered",
	},
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ["autodocs"],
} satisfies Meta<typeof BuyButton>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Main: Story = {
	args: {
		onClick: fn,
		items: [
			{
				label: "Button text",
				price: "price",
			},
			{
				label: "Button text",
				price: "Price",
				description: "An optional description",
				tags: ["optional tag"],
			},
			{
				label: "Button text",
				price: "Price",
				description: "Description only",
			},
			{
				label: "Button text",
				price: "Price",
				tags: ["only tag"],
			},
		],
	},
};
