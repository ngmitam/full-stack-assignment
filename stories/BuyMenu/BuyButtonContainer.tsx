"use client";
import BuyButton from "@/components/BuyButton";

import type { BuyButtonProps } from "@/components/BuyButton";
interface BuyButtonContainerProps {
	items: BuyButtonProps["items"];
}

export default function BuyButtonContainer({ items }: BuyButtonContainerProps) {
	return (
		<>
			<div style={{ margin: "5rem 0 10rem 25rem" }}>
				<BuyButton onClick={() => {}} items={items} />
			</div>
			<div style={{ margin: "10rem 0 5rem 25rem" }}>
				<BuyButton onClick={() => {}} items={items} />
			</div>
		</>
	);
}
