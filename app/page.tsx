import Image from "next/image";
import "./page.css";
import BuyButtonContainer from "@/stories/BuyMenu/BuyButtonContainer";

export default function Home() {
	return (
		<>
			<div className={"container"}>
				<div className="logo">
					<Image
						src="/next.svg"
						alt="Next.js Logo"
						width={180}
						height={37}
						priority
					/>
				</div>
				<BuyButtonContainer
					items={[
						{
							label: "Button text",
							price: "price",
						},
						{
							label: "Button text",
							price: "Price",
							description: "An optional description",
							tags: ["optional tag"],
						},
						{
							label: "Button text",
							price: "Price",
							description: "Description only",
						},
						{
							label: "Button text",
							price: "Price",
							tags: ["only tag"],
						},
					]}
				/>
			</div>
		</>
	);
}
