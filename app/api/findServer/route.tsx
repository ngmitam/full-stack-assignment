import { NextResponse } from "next/server";
const SERVERS = [
	{
		url: "https://does-not-work.perfume.new",
		priority: 1,
	},
	{
		url: "https://gitlab.com",
		priority: 4,
	},
	{
		url: "http://app.scnt.me",
		priority: 3,
	},
	{
		url: "https://offline.scentronix.com",
		priority: 2,
	},
];

export async function GET() {
	const promises: Promise<{
		url: string;
		priority: number;
		status: number;
	}>[] = [];
	for (const server of SERVERS) {
		promises.push(
			new Promise((resolve, reject) => {
				const controller = new AbortController();
				const signal = controller.signal;

				setTimeout(() => {
					controller.abort();
				}, 5000);

				fetch(server.url, { signal })
					.then((response) => {
						resolve({
							url: server.url,
							priority: server.priority,
							status: response.status,
						});
					})
					.catch((error) => {
						reject(error);
					});
			})
		);
	}

	let responses: PromiseSettledResult<{
		url: string;
		priority: number;
		status: number;
	}>[] = await Promise.allSettled(promises);

	let fulfilled: {
		url: string;
		priority: number;
		status: number;
	}[] = responses
		.filter((response) => response.status === "fulfilled")
		.map((response) => response.value);

	// status 200 - 299
	let success = fulfilled.filter(
		(response) => response.status >= 200 && response.status < 300
	);

	let sorted = success.sort((a, b) => {
		if (a.priority < b.priority) {
			return -1;
		}
		if (a.priority > b.priority) {
			return 1;
		}
		return 0;
	});

	if (sorted.length === 0) {
		return NextResponse.json({
			error: "All servers are down",
		});
	}

	return NextResponse.json({
		server: sorted[0].url,
	});
}
