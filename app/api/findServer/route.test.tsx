/**
 * @jest-environment node
 */

import { GET } from "./route";

describe("GET", () => {
	it("returns a 200", async () => {
		jest.spyOn(global, "fetch").mockImplementation(async (url) => {
			switch (url) {
				case "https://does-not-work.perfume.new":
					return Promise.resolve({ status: 500 });
				case "https://gitlab.com":
					return Promise.resolve({ status: 200 });
				case "http://app.scnt.me":
					return Promise.resolve({ status: 200 });
				case "https://offline.scentronix.com":
					return Promise.resolve({ status: 200 });
				default:
					return Promise.resolve({ status: 200 });
			}
		});
		const response = await GET();
		expect(response.status).toBe(200);
	}, 6000);

	it("returns right server", async () => {
		jest.spyOn(global, "fetch").mockImplementation(async (url) => {
			switch (url) {
				case "https://does-not-work.perfume.new":
					return Promise.resolve({ status: 500 });
				case "https://gitlab.com":
					return Promise.resolve({ status: 200 });
				case "http://app.scnt.me":
					return Promise.resolve({ status: 200 });
				case "https://offline.scentronix.com":
					return Promise.reject({ status: 200 });
				default:
					return Promise.resolve({ status: 200 });
			}
		});
		const response = await (await GET()).json();
		expect(response.server).toBe("http://app.scnt.me");
	}, 6000);

	it("all servers are down", async () => {
		jest.spyOn(global, "fetch").mockImplementation(async (url) => {
			switch (url) {
				case "https://does-not-work.perfume.new":
					return Promise.resolve({ status: 500 });
				case "https://gitlab.com":
					return Promise.resolve({ status: 500 });
				case "http://app.scnt.me":
					return Promise.resolve({ status: 500 });
				case "https://offline.scentronix.com":
					return Promise.resolve({ status: 500 });
				default:
					return Promise.resolve({ status: 500 });
			}
		});
		const response = await GET();
		expect(response.status).toBe(200);

		expect((await response.json()).error).toEqual("All servers are down");
	}, 6000);
});
